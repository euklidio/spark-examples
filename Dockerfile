FROM bde2020/spark-submit:2.1.0-hadoop2.8-hive-java8

RUN mkdir -p /app
COPY target/spark-examples-1.0-SNAPSHOT.jar /app/app.jar

ENV SPARK_MASTER_NAME spark-master
ENV SPARK_MASTER_PORT 7077
ENV SPARK_APPLICATION_JAR_LOCATION /app/app.jar
ENV SPARK_APPLICATION_ARGS ""