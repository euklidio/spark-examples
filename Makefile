build:
	mvn -DskipTests clean package
up:
	docker network create spark-net
	docker-compose -f docker-compose.yml up -d
down:
	docker-compose -f docker-compose.yml down
	docker-compose -f docker-compose-spark-app.yml down
	docker network rm spark-net
app:
	docker-compose -f docker-compose-spark-app.yml build
	docker-compose -f docker-compose-spark-app.yml up
copy: build
	docker cp target/sparkTutorial-1.0-SNAPSHOT.jar sparkapps_app:/app/app.jar