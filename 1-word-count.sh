#!/usr/bin/env bash

export SPARK_APPLICATION_MAIN_CLASS=com.epam.cigna.spark.samples.WordCount
mvn -DskipTests clean package
export MSYS_NO_PATHCONV=1
docker-compose -f docker-compose-spark-app.yml build
docker-compose -f docker-compose-spark-app.yml down


docker cp data/word_count.txt namenode:/
docker exec namenode hdfs dfs -mkdir -p /user/root
docker exec namenode hdfs dfs -rm -r data
docker exec namenode hdfs dfs -mkdir data
docker exec namenode hdfs dfs -rm -r data/word_count_output
docker exec namenode hdfs dfs -put /word_count.txt data

docker-compose -f docker-compose-spark-app.yml up