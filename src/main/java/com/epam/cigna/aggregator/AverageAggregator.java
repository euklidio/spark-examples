package com.epam.cigna.aggregator;

import com.epam.cigna.entity.Rating;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.expressions.Aggregator;

public class AverageAggregator extends Aggregator<Rating, Average, Double> {


    @Override
    public Average zero() {
        return new Average(0L, 0L);
    }

    @Override
    public Average reduce(Average b, Rating a) {
        long newSum = b.getSum() + a.getRating();
        long newCount = b.getCount() + 1;
        b.setSum(newSum);
        b.setCount(newCount);
        return b;
    }

    @Override
    public Average merge(Average b1, Average b2) {
        long newSum = b1.getSum() + b2.getSum();
        long newCount = b1.getCount() + b2.getCount();
        b1.setSum(newSum);
        b2.setCount(newCount);
        return b1;
    }

    @Override public Double finish(Average reduction) {
        return ((double) reduction.getSum()) / reduction.getCount();
    }

    @Override public Encoder<Average> bufferEncoder() {
        return Encoders.bean(Average.class);
    }

    @Override public Encoder<Double> outputEncoder() {
        return Encoders.DOUBLE();
    }
}


