package com.epam.cigna.streaming;

import com.epam.cigna.entity.Movie;
import com.epam.cigna.entity.Rating;
import com.epam.cigna.utils.SparkUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.Time;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;


public class WindowedStreaming {

    private SparkSession sparkSession;

    public static void main(String[] args) throws InterruptedException{
        WindowedStreaming sparkStreaming = new WindowedStreaming(SparkUtils.getSparkSession("Movie sql"));
        sparkStreaming.run();
    }

    public WindowedStreaming(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public void run() throws InterruptedException{
        Logger.getLogger("org").setLevel(Level.ERROR);

        JavaSparkContext sparkContext = new JavaSparkContext(sparkSession.sparkContext());
        JavaStreamingContext streamingContext = new JavaStreamingContext(sparkContext, Durations.seconds(3));
        JavaReceiverInputDStream<String> lines = streamingContext.socketTextStream("event_producer", 9999);
        JavaDStream<Integer> dStream = lines.map(Integer::valueOf);

        //max from last 3 seconds, computed every 12 seconds
        JavaDStream<Integer> dStream1 = dStream.reduceByWindow(Integer::max, Durations.seconds(3), Durations.seconds(12));
        dStream1.foreachRDD((integerJavaRDD, time) -> this.showWhole(integerJavaRDD, time, "Max"));

        streamingContext.start();
        streamingContext.awaitTermination();
    }

    private void showAverage(JavaRDD<Integer> integerJavaRDD, Time time, String type) {
        Dataset<Integer> dataset = sparkSession.createDataset(integerJavaRDD.rdd(), Encoders.INT());
        Dataset<Row> aggregated = dataset.groupBy().avg("value");
        System.out.println( type + " time: " + time);
        aggregated.show();
    }

    private void showWhole(JavaRDD<Integer> integerJavaRDD, Time time, String type) {
        Dataset<Integer> dataset = sparkSession.createDataset(integerJavaRDD.rdd(), Encoders.INT());
        System.out.println( type + " time: " + time);
        dataset.show();
    }
}
