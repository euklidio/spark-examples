package com.epam.cigna.streaming;

import com.epam.cigna.utils.SparkUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;



public class Streaming {

    private SparkSession sparkSession;

    public static void main(String[] args) throws InterruptedException{
        Streaming sparkStreaming = new Streaming(SparkUtils.getSparkSession("Movie sql"));
        sparkStreaming.run();
    }

    public Streaming(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public void run() throws InterruptedException{
        Logger.getLogger("org").setLevel(Level.ERROR);

        JavaSparkContext sparkContext = new JavaSparkContext(sparkSession.sparkContext());
        JavaStreamingContext streamingContext = new JavaStreamingContext(sparkContext, new Duration(1000));
        JavaReceiverInputDStream<String> lines = streamingContext.socketTextStream("event_producer", 9999);
        JavaDStream<Integer> map = lines.map(Integer::valueOf);
        map.foreachRDD((integerJavaRDD, time) -> {
            Dataset<Integer> dataset = sparkSession.createDataset(integerJavaRDD.rdd(), Encoders.INT());
            Dataset<Row> aggregated = dataset.groupBy().avg("value");
            System.out.println("Time: " + time);
            aggregated.show();
        });

        streamingContext.start();
        streamingContext.awaitTermination();
    }

}
