package com.epam.cigna.entity;

import java.io.Serializable;

public class Rating implements Serializable{
    private Integer id;
    private Byte rating;

    public Rating() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Rating withId(Integer id) {
        this.id = id;
        return this;
    }

    public Byte getRating() {
        return rating;
    }

    public void setRating(Byte rating) {
        this.rating = rating;
    }

    public Rating withRating(Byte rating) {
        this.rating = rating;
        return this;
    }
}
