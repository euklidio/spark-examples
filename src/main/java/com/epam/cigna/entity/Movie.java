package com.epam.cigna.entity;

import java.io.Serializable;

public class Movie implements Serializable {

    private Integer id;
    private String name;

    public Movie() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Movie withId(Integer id) {
        this.id = id;
        return this;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Movie withName(String name) {
        this.name = name;
        return this;
    }
}
