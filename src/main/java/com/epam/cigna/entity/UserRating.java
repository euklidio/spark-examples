package com.epam.cigna.entity;

public class UserRating {

    private Integer userId;
    private Integer movieId;
    private Byte rating;

    public UserRating() {
    }

    public Integer getUserId() {
        return userId;
    }

    public UserRating withUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieIdId(Integer id) {
        this.movieId = id;
    }

    public UserRating withMovieId(Integer id) {
        this.movieId = id;
        return this;
    }

    public Byte getRating() {
        return rating;
    }

    public void setRating(Byte rating) {
        this.rating = rating;
    }

    public UserRating withRating(Byte rating) {
        this.rating = rating;
        return this;
    }
}
