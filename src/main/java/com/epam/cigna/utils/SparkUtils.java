package com.epam.cigna.utils;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

public class SparkUtils {

    public static final String HADOOP_PATH_PREFIX = "hdfs://namenode:8020/user/root/";
    public static final String MOVIE_ITEM_FILE = HADOOP_PATH_PREFIX + "data/u.item";
    public static final String MOVIE_DATA_FILE = HADOOP_PATH_PREFIX + "data/u.data";

    public static JavaSparkContext getLocalContext(String appName) {
        SparkConf conf = new SparkConf().setAppName(appName).setMaster("local[*]");
        return new JavaSparkContext(conf);
    }

    public static SparkSession getSparkSession(String appName) {
        return SparkSession
                .builder()
                .appName(appName)
                .getOrCreate();
    }
}
