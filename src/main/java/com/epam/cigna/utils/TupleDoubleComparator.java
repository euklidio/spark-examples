package com.epam.cigna.utils;

import scala.Tuple2;

import java.io.Serializable;
import java.util.Comparator;

public class TupleDoubleComparator<S> implements Comparator<Tuple2<Double, S>>, Serializable {
    @Override
    public int compare(Tuple2<Double, S> o1, Tuple2<Double, S> o2) {
        return -1 * Double.compare(o1._1, o2._1);
    }
}