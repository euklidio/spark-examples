package com.epam.cigna.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.stream.IntStream;

public class Merger {
    public static void main(String[] args) throws IOException {
        File fileName = new File(Merger.class.getClassLoader().getResource("fileName").getFile());
        File input = new File("data/veryBig.txt");
        File output = new File("data/output.txt");

        String inputString = FileUtils.readFileToString(input);

        IntStream.range(1,200).forEach(value -> {
            try {
                FileUtils.write(output, inputString, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
