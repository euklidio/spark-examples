package com.epam.cigna.spark.samples;

import com.epam.cigna.utils.SparkUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Pi {
    public static void main(String[] args) {
        SparkSession spark = SparkUtils.getSparkSession("JavaSparkPI");
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

        int n = 500000;
        List<Integer> integerList = IntStream.range(1, n).boxed().collect(Collectors.toList());

        JavaRDD<Integer> dataSet = jsc.parallelize(integerList);

        int count = dataSet.map(integer -> {
            double x = ThreadLocalRandom.current().nextDouble(-1, 1);
            double y = ThreadLocalRandom.current().nextDouble(-1, 1);
            return (x * x + y * y <= 1) ? 1 : 0;
        }).reduce((integer, integer2) -> integer + integer2);

        System.out.println("Pi is roughly " + 4.0 * count / n);

        spark.stop();
    }
}
