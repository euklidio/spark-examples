package com.epam.cigna.spark.samples;

import com.epam.cigna.utils.SparkUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.util.Arrays;

public class WordCount {
    public static void main(String[] args) {
        SparkSession spark = SparkUtils.getSparkSession("WordCount");
        JavaSparkContext sc = new JavaSparkContext(spark.sparkContext());

        JavaRDD<String> lines = sc.textFile("hdfs://namenode:8020/user/root/data/word_count.txt");
        JavaPairRDD<String, Integer> wordCounts = lines.flatMap(line -> Arrays.asList(line.split(" ")).iterator())
                .mapToPair(word -> new Tuple2<>(word, 1))
                .reduceByKey((a, b) -> a + b)
                .mapToPair(Tuple2::swap)
                .sortByKey()
                .mapToPair(Tuple2::swap);

        wordCounts.saveAsTextFile("hdfs://namenode:8020/user/root/data/word_count_output");
    }
}
