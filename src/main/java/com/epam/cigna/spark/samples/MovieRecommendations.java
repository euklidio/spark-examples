package com.epam.cigna.spark.samples;

import com.epam.cigna.entity.Movie;
import com.epam.cigna.entity.UserRating;
import com.epam.cigna.utils.SparkUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.recommendation.ALS;
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel;
import org.apache.spark.mllib.recommendation.Rating;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import static com.epam.cigna.utils.SparkUtils.*;

public class MovieRecommendations {

    private SparkSession sparkSession;

    public static void main(String[] args) {
        MovieRecommendations movieCount = new MovieRecommendations(SparkUtils.getSparkSession("Movie recommendations"));
        movieCount.run();
    }

    public MovieRecommendations(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public void run() {
        JavaSparkContext sparkContext = new JavaSparkContext(sparkSession.sparkContext());
        JavaRDD<Movie> movies = sparkContext.textFile(HADOOP_PATH_PREFIX + "data/movies/u.item").map(MovieRecommendations::getMovieFromLine);
        JavaRDD<UserRating> ratings = sparkContext.textFile(HADOOP_PATH_PREFIX + "data/movies/u.data").map
                (MovieRecommendations::getRatingFromLine);

        Map<Integer, String> moviesMap = movies.collect().stream().collect(Collectors.toMap(Movie::getId, Movie::getName));

        RDD<Rating> toTrain = ratings.map(userRating -> new Rating(userRating.getUserId(), userRating.getMovieId(), (double) userRating.getRating())).rdd();

        MatrixFactorizationModel model = ALS.train(toTrain, 8, 20);


        Arrays.asList(model.recommendProducts(0, 10)).forEach(rating -> {
            System.out.println(moviesMap.get(rating.product()) + " :" + rating.rating());
        });

    }


    private static Movie getMovieFromLine(String line) throws Exception {
        String[] lineTokens = line.split("\\|");
        return new Movie().withId(Integer.valueOf(lineTokens[0])).withName(lineTokens[1]);
    }


    private static UserRating getRatingFromLine(String line) {
        String[] lineTokens = line.split("\\s+");
        return new UserRating().withMovieId(Integer.valueOf(lineTokens[1])).withRating(Byte.valueOf(lineTokens[2])).withUserId(Integer.valueOf(lineTokens[0]));
    }
}
