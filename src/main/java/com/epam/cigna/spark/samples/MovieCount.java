package com.epam.cigna.spark.samples;

import com.epam.cigna.utils.SparkUtils;
import com.epam.cigna.utils.TupleDoubleComparator;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import static com.epam.cigna.utils.SparkUtils.*;

public class MovieCount {

    private SparkSession sparkSession;

    public static void main(String[] args) {
        MovieCount movieCount = new MovieCount(SparkUtils.getSparkSession("Movie count"));
        movieCount.run();
    }

    public MovieCount(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public void run() {
        JavaSparkContext sparkContext = new JavaSparkContext(sparkSession.sparkContext());
        JavaPairRDD<Integer, String> movies = loadMovies(sparkContext, MOVIE_ITEM_FILE);
        JavaPairRDD<Integer, Tuple2<Double, Double>> ratings = loadRatings(sparkContext, MOVIE_DATA_FILE);

        JavaPairRDD<Integer, Tuple2<Double, Double>> aggregatedRatings = ratings.reduceByKey((v1, v2) -> new Tuple2<>(v1._1 + v2._1, v1._2 + v2._2))
                .filter(v1 -> v1._2._2 > 20);

        JavaPairRDD<Integer, Double> averageValues = aggregatedRatings.mapValues(v1 -> {
            return v1._1 / v1._2;
        });

        JavaPairRDD<Integer, Tuple2<Double, String>> joined = averageValues.join(movies);

        JavaPairRDD<Integer, Tuple2<Double, String>> sorted = joined.mapToPair(Tuple2::swap).sortByKey(new TupleDoubleComparator<>()).mapToPair
                (Tuple2::swap);
        sorted.take(10).forEach(System.out::println);

    }

    private JavaPairRDD<Integer, String> loadMovies(JavaSparkContext context, String filename) {
        return context.textFile(filename)
                .mapToPair(MovieCount::getMovieFromLine);
    }

    private static Tuple2<Integer, String> getMovieFromLine(String line) throws Exception {
        String[] lineTokens = line.split("\\|");
        return new Tuple2<>(Integer.valueOf(lineTokens[0]), lineTokens[1]);
    }

    private JavaPairRDD<Integer, Tuple2<Double, Double>> loadRatings(JavaSparkContext context, String
            filename) {

        return context.textFile(filename).mapToPair(MovieCount::getRatingFromLine);
    }

    private static Tuple2<Integer, Tuple2<Double, Double>> getRatingFromLine(String line) {
        String[] lineTokens = line.split("\\s+");
        return new Tuple2<>(Integer.valueOf(lineTokens[1]), new Tuple2<>(Double.valueOf(lineTokens[2]),
                1.0));
    }
}
