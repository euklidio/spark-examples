package com.epam.cigna.spark.samples;

import com.epam.cigna.entity.Movie;
import com.epam.cigna.entity.Rating;
import com.epam.cigna.utils.SparkUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static com.epam.cigna.utils.SparkUtils.*;
import static org.apache.spark.sql.functions.*;

public class MovieDataSet {

    private SparkSession sparkSession;

    public static void main(String[] args) {
        MovieDataSet movieDataSet = new MovieDataSet(SparkUtils.getSparkSession("Movie data set"));
        movieDataSet.run();
    }

    public MovieDataSet(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public void run() {
        JavaSparkContext sparkContext = new JavaSparkContext(sparkSession.sparkContext());
        JavaRDD<Movie> movies = sparkContext.textFile(MOVIE_ITEM_FILE).map(MovieDataSet::getMovieFromLine);
        JavaRDD<Rating> ratings = sparkContext.textFile(MOVIE_DATA_FILE).map(MovieDataSet::getRatingFromLine);


        Encoder<Rating> ratingEncoder = Encoders.bean(Rating.class);
        Encoder<Movie> movieEncoder = Encoders.bean(Movie.class);

        Dataset<Rating> ratingDS = sparkSession.createDataset(ratings.rdd(), ratingEncoder);
        Dataset<Movie> movieDS = sparkSession.createDataset(movies.rdd(), movieEncoder);

        Dataset<Row> join = ratingDS.groupBy("id").agg(avg("rating"), count("id")).join(movieDS, "id").filter(col("count(id)").gt(5)).orderBy(desc("avg(rating)"));
        join.show();
    }


    private static Movie getMovieFromLine(String line) throws Exception {
        String[] lineTokens = line.split("\\|");
        return new Movie().withId(Integer.valueOf(lineTokens[0])).withName(lineTokens[1]);
    }


    private static Rating getRatingFromLine(String line) {
        String[] lineTokens = line.split("\\s+");
        return new Rating().withId(Integer.valueOf(lineTokens[1])).withRating(Byte.valueOf(lineTokens[2]));
    }
}
