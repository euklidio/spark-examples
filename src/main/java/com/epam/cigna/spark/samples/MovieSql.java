package com.epam.cigna.spark.samples;

import com.epam.cigna.entity.Movie;
import com.epam.cigna.entity.Rating;
import com.epam.cigna.utils.SparkUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static com.epam.cigna.utils.SparkUtils.*;

public class MovieSql {

    private SparkSession sparkSession;

    public static void main(String[] args) {
        MovieSql movieSql = new MovieSql(SparkUtils.getSparkSession("Movie sql"));
        movieSql.run();
    }

    public MovieSql(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public void run() {
        JavaSparkContext sparkContext = new JavaSparkContext(sparkSession.sparkContext());
        JavaRDD<Movie> movies = sparkContext.textFile(MOVIE_ITEM_FILE).map(MovieSql::getMovieFromLine);
        JavaRDD<Rating> ratings = sparkContext.textFile(MOVIE_DATA_FILE).map
                (MovieSql::getRatingFromLine);
        Dataset<Row> movieDF = sparkSession.createDataFrame(movies, Movie.class);
        Dataset<Row> ratingDF = sparkSession.createDataFrame(ratings, Rating.class);
        movieDF.createOrReplaceTempView("MOVIE");
        ratingDF.createOrReplaceTempView("RATING");
        Dataset<Row> theBestRating = sparkSession.sql("select name, avg from (SELECT r.id as id, count(*) as sum, avg(r.rating) as avg from RATING r " +
                "group by r.id) AverageRating join MOVIE on AverageRating.id=Movie.id where sum > 20 order by avg desc");
        theBestRating.show();

    }


    private static Movie getMovieFromLine(String line) throws Exception {
        String[] lineTokens = line.split("\\|");
        return new Movie().withId(Integer.valueOf(lineTokens[0])).withName(lineTokens[1]);
    }


    private static Rating getRatingFromLine(String line) {
        String[] lineTokens = line.split("\\s+");
        return new Rating().withId(Integer.valueOf(lineTokens[1])).withRating(Byte.valueOf(lineTokens[2]));
    }
}
