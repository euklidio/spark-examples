package com.epam.cigna.spark.samples;

import com.epam.cigna.aggregator.BetterAverage;
import com.epam.cigna.entity.Movie;
import com.epam.cigna.entity.Rating;
import com.epam.cigna.utils.SparkUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static com.epam.cigna.utils.SparkUtils.*;

public class MovieSqlAggregator {

    private SparkSession sparkSession;

    public static void main(String[] args) throws AnalysisException  {
        MovieSqlAggregator movieSqlAggegator = new MovieSqlAggregator(SparkUtils.getSparkSession("Movie sql aggregator"));
        movieSqlAggegator.run();
    }

    public MovieSqlAggregator(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public void run() throws AnalysisException {
        JavaSparkContext sparkContext = new JavaSparkContext(sparkSession.sparkContext());
        JavaRDD<Movie> movies = sparkContext.textFile(MOVIE_ITEM_FILE).map(MovieSqlAggregator::getMovieFromLine);
        JavaRDD<Rating> ratings = sparkContext.textFile(MOVIE_DATA_FILE).map
                (MovieSqlAggregator::getRatingFromLine);
        Dataset<Row> movieDF = sparkSession.createDataFrame(movies, Movie.class);
        Dataset<Row> ratingDF = sparkSession.createDataFrame(ratings, Rating.class);
        movieDF.createOrReplaceTempView("MOVIE");
        ratingDF.createOrReplaceTempView("RATING");
        sparkSession.udf().register("betterAverage", new BetterAverage(10));

        Dataset<Row> theBestRating = sparkSession.sql("SELECT m.name, betterAverage(r.rating) as avg from RATING r " +
                " join MOVIE m on r.id = m.id group by m.name order by avg desc");
        theBestRating.show();

    }


    private static Movie getMovieFromLine(String line) throws Exception {
        String[] lineTokens = line.split("\\|");
        return new Movie().withId(Integer.valueOf(lineTokens[0])).withName(lineTokens[1]);
    }


    private static Rating getRatingFromLine(String line) {
        String[] lineTokens = line.split("\\s+");
        return new Rating().withId(Integer.valueOf(lineTokens[1])).withRating(Byte.valueOf(lineTokens[2]));
    }
}
