#!/usr/bin/env bash

docker-compose -f docker-compose.yml down
docker-compose -f docker-compose-spark-app.yml down
docker network rm spark-net
