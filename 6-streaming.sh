#!/usr/bin/env bash

export SPARK_APPLICATION_MAIN_CLASS=com.epam.cigna.streaming.Streaming
mvn -DskipTests clean package
export MSYS_NO_PATHCONV=1
docker-compose -f docker-compose-spark-app.yml build
docker-compose -f docker-compose-spark-app.yml down
docker-compose -f docker-compose-spark-app.yml up