#!/usr/bin/env bash
mvn -DskipTests clean package
export MSYS_NO_PATHCONV=1
docker-compose -f docker-compose-spark-app.yml build
docker-compose -f docker-compose-spark-app.yml down

docker cp data/movies/u.data namenode:/u.data
docker cp data/movies/u.item namenode:/u.item

docker exec namenode hdfs dfs -mkdir -p /user/root
docker exec namenode hdfs dfs -mkdir data
docker exec namenode hdfs dfs -put /u.data data
docker exec namenode hdfs dfs -put /u.item data

docker-compose -f docker-compose-spark-app.yml up