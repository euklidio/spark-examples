#!/usr/bin/env bash

docker-compose -f docker-compose-streaming-app.yml down
docker-compose -f docker-compose-spark-app.yml down
