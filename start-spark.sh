#!/usr/bin/env bash

./stop-spark.sh

docker network create spark-net
docker-compose -f docker-compose.yml up -d